#include <ArduinoConsole.h>

// create an ArduinoConsole instance
ArduinoConsole console;

void setup() {
    // Init serial that the console uses
    Serial.begin(9600);
}

void loop() {
    // Run console frame
    console.run();
}