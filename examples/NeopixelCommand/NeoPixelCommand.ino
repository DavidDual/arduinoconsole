
#ifdef ARDUINO_SAM_DUE

#include <DueTimer.h>

#else

#include "TimerOne.h"

#endif

#include <ArduinoConsole.h>
#include <Command/NeoPixel.h>

// create an ArduinoConsole instance
ArduinoConsole console;
// create an NeoPixelCommand instance. NeoPixelCommand is an instance of ArduinoCommand that controls Adafruit_NeoPixel:
//     Parameter 1 = number of pixels in neoStrip,  neopixel stick has 8
//     Parameter 2 = pin number (most are valid)
//     Parameter 3 = pixel type flags, add together as needed:
//       NEO_RGB     Pixels are wired for RGB bitstream
//       NEO_GRB     Pixels are wired for GRB bitstream, correct for neopixel stick
//       NEO_KHZ400  400 KHz bitstream (e.g. FLORA pixels)
//       NEO_KHZ800  800 KHz bitstream (e.g. High Density LED neoStrip), correct for neopixel stick
NeoPixelCommand neo(100, 6, NEO_RGB + NEO_KHZ800);

void setup() {
    // Init serial that the console uses
    Serial.begin(9600);
    // Add command Neo to command list
    console.addCommand(&neo);
    // Init Adafruit_NeoPixel
    neo.begin();
    // Run console frame every microsecond
    Timer0.attachInterrupt([&]() { console.run(); }).setPeriod(1000).start();
}

void loop() {
}