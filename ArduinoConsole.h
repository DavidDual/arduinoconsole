//
// Created by david on 6/7/2016.
//

#ifndef TEST_ARDUINOCONSOLE_H
#define TEST_ARDUINOCONSOLE_H

#include <Arduino.h>
#include <UARTClass.h>
#include "Vector.h"
#include "ArduinoCommand.h"

class ArduinoConsole : public ArduinoCommand {
    Stream &shellSerial;

    int historyPos = 0;
    Vector<String> history;
    Vector<ArduinoCommand *> commands;

    bool enter = true;

    bool Read(String &data);

public:
    ArduinoConsole(Stream &shellSerial = Serial);

    void addCommand(ArduinoCommand &command);

    void run();

    void handle();
};


#endif //TEST_ARDUINOCONSOLE_H
