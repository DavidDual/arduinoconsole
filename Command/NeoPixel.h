//
// Created by david on 5/11/2017.
//

#ifndef ARDUINO_NEOPIXEL_H
#define ARDUINO_NEOPIXEL_H


#include <Adafruit_NeoPixel.h>
#include <stdint.h>
#include "ArduinoCommand.h"

class NeoPixelCommand: public ArduinoCommand {
// Parameter 1 = number of pixels in neoStrip,  neopixel stick has 8
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_RGB     Pixels are wired for RGB bitstream
//   NEO_GRB     Pixels are wired for GRB bitstream, correct for neopixel stick
//   NEO_KHZ400  400 KHz bitstream (e.g. FLORA pixels)
//   NEO_KHZ800  800 KHz bitstream (e.g. High Density LED neoStrip), correct for neopixel stick
    Adafruit_NeoPixel neoPixel;

    void clear();

    void setBrightness(uint8_t brightness);

    uint32_t getColor(String *color);

    void ColorWipe(Vector<String> *colors, Vector<String> *interval);

    void staticColor(Vector<String> *colorsl);

public:
    NeoPixelCommand(uint16_t pixelCount, uint8_t pixelPin=6, neoPixelType type=NEO_GRB + NEO_KHZ800);

    void begin();

    Adafruit_NeoPixel *getNeoPixel();

    void handle();
};

#endif //ARDUINO_NEOPIXEL_H
