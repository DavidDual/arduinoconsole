//
// Created by david on 5/11/2017.
//

#include "ArduinoCommand.h"

Vector<String> ArduinoCommandTemp;
Vector<String> ArduinoCommandTemp1;

ArduinoCommand::ArduinoCommand(const char signature[], const char description[]) : signature(signature),
                                                                                   description(description) {
    ArduinoCommandTemp.clear();
    explode(' ', this->signature, &ArduinoCommandTemp);
    command = ArduinoCommandTemp[0];
    for (int i = 1; i < ArduinoCommandTemp.size(); ++i) {
        if (ArduinoCommandTemp[i].substring(0, 1) == "-") {
            ArduinoCommandTemp1.clear();
            explode('=', ArduinoCommandTemp[i], &ArduinoCommandTemp1);
            options.push_back(Option(ArduinoCommandTemp1[0]));
            if (ArduinoCommandTemp1.size() > 1) {
                explode(',', ArduinoCommandTemp1[1], &options[options.size() - 1].defaultValue);
            }
        } else {
            if (ArduinoCommandTemp[i].substring(ArduinoCommandTemp[i].length() - 1,
                                                ArduinoCommandTemp[i].length() - 1) == "?") {
                ArduinoCommandTemp[i].remove(ArduinoCommandTemp[i].length() - 1, ArduinoCommandTemp[i].length() - 1);
                arguments.push_back(Argument(ArduinoCommandTemp[i], true));
            } else {
                if (arguments.size() > 0 && arguments[arguments.size() - 1].optional) {
                    shellSerial.println("Cannot add a required argument after an optional one.");
                    while (true);
                }
                arguments.push_back(Argument(ArduinoCommandTemp[i], false));
            }
        }
    }
}

void ArduinoCommand::explode(const char &delimiter, String &haystack, Vector<String> *returnData) {
    haystack.trim();
    int begin = 0, end = haystack.indexOf(delimiter);
    while (end > 0) {
        returnData->push_back(haystack.substring(begin, (unsigned int) end));
//        haystack.remove(0, (unsigned int) end + 1);
        begin = end + 1;
        end = haystack.indexOf(delimiter, begin);
    }
    returnData->push_back(haystack.substring(begin, haystack.length()));
}

bool ArduinoCommand::parseInput(Vector<String> &data) {
    int arg = 0;
    for (int k = 0; k < options.size(); ++k) {
        if (options[k].value.size() > 0) {
            options[k].value.clear();
        }
        if (options[k].available) {
            options[k].available = false;
        }
    }
    for (int i = 1; i < data.size(); ++i) {
        if (data[i].substring(0, 1) == String('-')) {
            bool op = false;
            ArduinoCommandTemp.clear();
            explode('=', data[i], &ArduinoCommandTemp);
            if (ArduinoCommandTemp[0] == "-h") {
                printHelp();
                return false;
            }
            for (int k = 0; k < options.size(); ++k) {
                if (options[k].name == ArduinoCommandTemp[0]) {
                    if (ArduinoCommandTemp.size() > 1) {
                        explode(',', ArduinoCommandTemp[1], &options[k].value);
                    }
                    options[k].available = true;
                    op = true;
                    break;
                }
            }
            if (!op) {
                shellSerial.print("Unknown option: ");
                shellSerial.println(data[i]);
                return false;
            }
        } else {
            if (arg >= arguments.size()) {
                shellSerial.println("Too many arguments.");
                return false;
            }
            arguments[arg].value = data[i];
            arg++;
        }
    }
    if (arg < arguments.size() && !arguments[arg].optional) {
        shellSerial.print("Not enough arguments (missing: ");
        shellSerial.print(arguments[arg].name);
        while (++arg < arguments.size() && !arguments[arg].optional) {
            shellSerial.print(", ");
            shellSerial.print(arguments[arg].name);
        }
        shellSerial.println(")");
        return false;
    }
    for (; arg < arguments.size(); ++arg) {
        if (arguments[arg].value.length() > 0) {
            arguments[arg].value = "";
        }
    }
    return true;
}

bool ArduinoCommand::hasOption(const String &option) {
    for (int k = 0; k < options.size(); ++k) {
        if (options[k].name == option) {
            return options[k].available;
        }
    }
    return false;
}

Vector<String> *ArduinoCommand::getOption(const String &option) {
    for (int k = 0; k < options.size(); ++k) {
        if (options[k].name == option) {
            if (options[k].value.size() > 0) {
                return &options[k].value;
            }
            return &options[k].defaultValue;
        }
    }
    return nullptr;
}

const Vector<Option> &ArduinoCommand::getOptionNames() {
    return options;
}

String *ArduinoCommand::getArgument(const char *argument) {
    for (int k = 0; k < arguments.size(); ++k) {
        if (arguments[k].name == argument) {
            return &arguments[k].value;
        }
    }
    return nullptr;
}

const Vector<Argument> &ArduinoCommand::getArgumentNames() {
    return arguments;
}

String ArduinoCommand::getDescription() {
    return description;
}
void ArduinoCommand::setSerial(Stream &shellSerial) {
    this->shellSerial = shellSerial;
}

void ArduinoCommand::printHelp() {
    shellSerial.print(getCommand());
    Vector<Argument> names = getArgumentNames();
    for (int j = 0; j < names.size(); ++j) {
        shellSerial.print(" ");
        shellSerial.print(names[j].name);
    }
    shellSerial.println();
    shellSerial.println("Options:");
    Vector<Option> opNames = getOptionNames();
    for (int j = 0; j < opNames.size(); ++j) {
        shellSerial.print(opNames[j].name);
        shellSerial.print("[=");
        shellSerial.print(opNames[j].name);
        shellSerial.print("]");
        if (opNames[j].defaultValue.size() > 0) {
            shellSerial.print(" Default:");
            shellSerial.print(opNames[j].defaultValue[0]);
            for (int i = 1; i < opNames[j].defaultValue.size(); ++i) {
                shellSerial.print(",");
                shellSerial.print(opNames[j].defaultValue[i]);
            }
        }
        shellSerial.println();
    }
    shellSerial.println(getDescription());
}
