//
// Created by david on 6/7/2016.
//
#include "ArduinoConsole.h"

ArduinoConsole::ArduinoConsole(Stream &shellSerial) :
        ArduinoCommand("help", "Command description"),
        shellSerial(shellSerial) {
    history.push_back("");
    addCommand(*this);
}

void ArduinoConsole::handle() {
    shellSerial.println("Commands:");
    for (int i = 0; i < commands.size(); ++i) {
        shellSerial.println(commands[i]->getCommand());
    }
}

void ArduinoConsole::addCommand(ArduinoCommand &command) {
    command.setSerial(shellSerial);
    this->commands.push_back(&command);
}

int n;

bool ArduinoConsole::Read(String &data) {
    n = shellSerial.read();
    if (n != -1) {
        if (n == 13) {
            shellSerial.println();
            return true;
        }
        if (n == 127 && data.length() > 0) {
            data.remove(data.length() - 1);
            shellSerial.print((char) n);
        } else if (n == 65) {    // UP
            if (historyPos > 0) {
                for (unsigned int i = 0; i < history[history.size() - 1].length(); ++i) {
                    shellSerial.print((char) 127);
                }
                shellSerial.print(history[--historyPos]);
                history[history.size() - 1] = history[historyPos];
            }
        } else if (n == 66) {    // Down
            if (historyPos < history.size() - 1) {
                for (unsigned int i = 0; i < history[history.size() - 1].length(); ++i) {
                    shellSerial.print((char) 127);
                }
                if (++historyPos == history.size() - 1) {
                    history[history.size() - 1] = "";
                } else {
                    shellSerial.print(history[historyPos]);
                    history[history.size() - 1] = history[historyPos];
                }
            }
        } else if (n == 67) {    // Right
        } else if (n == 68) {    // Left
        } else if (n != 127 && n != 27 && n != 91) {
            data += (char) n;
            shellSerial.print((char) n);
        }
    }
    return false;
}

Vector<String> param;

void ArduinoConsole::run() {
    if (enter) {
        enter = false;
        shellSerial.print("\nArduino:\t");
        historyPos = history.size() - 1;
        if (history[historyPos].length() > 0) {
            if (history.size() > 1 && history[history.size() - 1] == history[history.size() - 2]) {
                history[history.size() - 1] = "";
            } else {
                historyPos++;
                history.push_back("");
            }
        }
    }
    if (Read(history[history.size() - 1])) {
        enter = true;

        if (history[history.size() - 1].length() > 0) {
            param.clear();
            explode(' ', history[history.size() - 1], &param);
            for (int i = 0; i < commands.size(); ++i) {
                if (param[0] == commands[i]->getCommand()) {
                    if (commands[i]->parseInput(param)) {
                        commands[i]->handle();
                    }
                    return;
                }
            }
            shellSerial.print("Command ");
            shellSerial.print(param[0]);
            shellSerial.println(" not fount.");
        }
    }
}