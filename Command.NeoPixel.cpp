//
// Created by david on 5/11/2017.
//

#ifdef ARDUINO_SAM_DUE

#include <DueTimer.h>

#else

#include "TimerOne.h"

#endif

#include <Command/NeoPixel.h>

NeoPixelCommand::NeoPixelCommand(uint16_t pixelCount, uint8_t pixelPin, neoPixelType type) : ArduinoCommand("Neo Type --color=red,blue --interval=120 --brightness=255",
                                                    "Neo Pixel control command."),
                                                                                             neoPixel(pixelCount, pixelPin, type) {}
Adafruit_NeoPixel *neoStrip;

void NeoPixelCommand::begin() {
    neoStrip = &neoPixel;
    neoStrip->begin();
    neoStrip->show(); // Initialize all pixels to 'off'
}

int ColorWipeColor = 0;
Vector <uint32_t> ColorWipeColors;
uint16_t ColorWipeIndex = 0;

void NeoPixelCommand::ColorWipe(Vector <String> *colors, Vector <String> *interval) {
    //clear();
    ColorWipeColor = 0;
    ColorWipeIndex = 0;
    ColorWipeColors.clear();
    for (int i = 0; i < (*colors).size(); ++i) {
        ColorWipeColors.push_back(getColor(&(*colors)[i]));
    }
    Timer1.attachInterrupt([&]() {
        neoStrip->setPixelColor(ColorWipeIndex, ColorWipeColors[ColorWipeColor]);
        neoStrip->show();
        ColorWipeIndex++;
        if (ColorWipeIndex >= neoStrip->numPixels()) {
            ColorWipeIndex = 0;
            ColorWipeColor++;
            if (ColorWipeColor >= ColorWipeColors.size()) {
                ColorWipeColor = 0;
            }
        }
    }).setPeriod((*interval)[0].toInt()).start();
}

void NeoPixelCommand::staticColor(Vector <String> *colors) {
    clear();
    uint32_t color = getColor(&(*colors)[0]);
    for (int i = 0; i < neoStrip->numPixels(); ++i) {
        neoStrip->setPixelColor((uint16_t) i, color);
    }
    neoStrip->show();
}

Vector <String> NeoPixelTemp;

uint32_t NeoPixelCommand::getColor(String *color) {
    if (*color == "blue") {
        return neoStrip->Color(255, 0, 0);
    }
    if (*color == "red") {
        return neoStrip->Color(0, 255, 0);
    }
    if (*color == "green") {
        return neoStrip->Color(0, 0, 100);
    }
    NeoPixelTemp.clear();
    explode('.', *color, &NeoPixelTemp);
    if (NeoPixelTemp.size() == 3) {
        return neoStrip->Color(NeoPixelTemp[2].toInt(), NeoPixelTemp[0].toInt(), NeoPixelTemp[1].toInt());
    }
    return neoStrip->Color(0, 0, 0);
}

Adafruit_NeoPixel *NeoPixelCommand::getNeoPixel() {
    return &neoPixel;
}

void NeoPixelCommand::clear() {
    Timer1.stop();
    for (int i = 0; i < neoStrip->numPixels(); i++) {
        neoStrip->setPixelColor(i, 0, 0, 0);
    }
    neoStrip->show(); // Initialize all pixels to 'off'
}

void NeoPixelCommand::setBrightness(uint8_t brightness) {
    neoStrip->setBrightness(brightness);
}

void NeoPixelCommand::handle() {
    String *type = getArgument("Type");
    if (hasOption("--brightness")) {
        setBrightness((*getOption("--brightness"))[0].toInt());
    }
    if (*type == "wipe") {
        ColorWipe(getOption("--color"), getOption("--interval"));
    } else if (*type == "static") {
        staticColor(getOption("--color"));
    }
}
