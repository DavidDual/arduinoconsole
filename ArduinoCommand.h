//
// Created by david on 5/11/2017.
//

#ifndef ARDUINO_ARDUINOCOMMAND_H
#define ARDUINO_ARDUINOCOMMAND_H

#include "Arduino.h"
#include "Vector.h"

struct Argument {
    String name;
    String value;
    bool optional = false;

    Argument(String &name, bool optional) : name(name), optional(optional) {}
};

struct Option {
    String name;
    Vector<String> value;
    Vector<String> defaultValue;
    bool available = false;

    Option(String &name) : name(name) {}
};

class ArduinoCommand {
    Stream &shellSerial = Serial;
    String command;
    Vector<Argument> arguments;
    Vector<Option> options;
    String signature;
    String description;

    void printHelp();

public:
    void explode(const char &delimiter, String &haystack, Vector<String> *returnData);

    ArduinoCommand(const char signature[], const char description[]);

    virtual void handle() = 0;

    String &getCommand() { return command; };

    bool parseInput(Vector<String> &data);

    bool hasOption(const String &option);

    Vector<String> *getOption(const String &option);

    const Vector<Option> &getOptionNames();

    String *getArgument(const char *argument);

    const Vector<Argument> &getArgumentNames();

    String getDescription();

    void setSerial(Stream &shellSerial);
};

#endif //ARDUINO_ARDUINOCOMMAND_H
